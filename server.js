var config = require('./config');

require('./common/helpers/cluster-node')(config, {
    workers: {
        front_end: {
            workerSrc: './webapp/worker',
            count: config.app.workersCount
        },
        http_api: {
            workerSrc: './api/web-worker',     //root relative path
            count: config.api.workersCount
        },
        kue_processor: {
            workerSrc: './rate-processor/kue-processor'
        },
        kue_gui: {
            workerSrc: './rate-processor/kue-gui'
        }
    },
    autoRestart: !config.dev
});
