#!/usr/bin/env node

var program = require('commander'),
    checkPosters = require('./tasks/task_check_posters_on_fs'),
    queueMissedPosters = require('./tasks/task_queue_missed_posters'),
    checkQueueMissedFields = require('./tasks/task_queue_with_missed_fileds'),
    updateMovies = require('./tasks/task_update_movies');


var config = require('../config'),
    crawler_config = require('../_phantom-crawler/crawler-config');
config.crawler = crawler_config;


program
    .version('0.1');

program
    .command('fill-posters-from-fs')
    .description('check posters files on disk and update movies DB')
    .action(function(){
        checkPosters(config, function(){
            console.log('Done');
            process.exit();
        });
    });

program
    .command('queue-missed-posters')
    .description('start download missed posters')
    .option('-n, --number <n>', 'number of posters to process', parseInt)
    .action(function(){
        queueMissedPosters(config, program.args[0].number, function(){
            console.log('Done');
            setTimeout(function(){
                process.exit();
            }, 1000);
        });
    }).on('--help', function() {
        console.log('  Examples:');
        console.log();
        console.log('    $ do.js queue-missed-posters -n 100');
        console.log();
    });

program
    .command('queue-missed-fields')
    .description('Queue Missed Fields')
    .option('-n, --number <n>', 'number of movies to process', parseInt)
    .option('-f, --field <n>', 'search for absent field ')
    .action(function(){
        console.log(program.args[0].field);
        checkQueueMissedFields(config, program.args[0].number, program.args[0].field, function(){
            console.log('Done');
            setTimeout(function(){
                process.exit();
            }, 1000);
        });
    }).on('--help', function() {
        console.log('  Examples:');
        console.log();
        console.log('    $ do.js queue-missed-fields -n 100 -f writers');
        console.log();
    });

program
    .command('update-movies')
    .description('Update movies according to query string')
    .option('-d, --dry ', 'dry run')
    .option('-q, --query <s>', 'mongodb query')
    .action(function(){
        //console.log(program.args[0].dry);
        //console.log(program.args[0].query);
        updateMovies(config, program.args[0].query, program.args[0].dry, function(){
            console.log('Done');
            setTimeout(function(){
                process.exit();
            }, 1000);
        });

    }).on('--help', function() {
        console.log('  Examples:');
        console.log();
        //console.log('    $ do.js queue-missed-fields -n 100 -f writers');
        console.log();
    });

program.parse(process.argv);
if (!program.args.length) program.help();
