/**
 * User: gkoy
 * Date: 26.09.13
 * Time: 16:31
 */
var fs = require('fs'),
    mongoose = require('mongoose'),
    mongooseInit = require('../../common/helpers/mongoose-init'),
    PathService = require('../../common/services/path-service');

module.exports = function (config, done) {

    var pathService = new PathService(config);

    // Bootstrap db connection & models
    mongooseInit(config, {
        models_path: 'common/model',
        models: ['movie']
    }, function(){
        console.log('check posters');

        var Movie = mongoose.model('Movie');

        var movieStream = Movie.find().stream();

        movieStream.on('data', function (movie) {
            var paths = pathService.getMoviePosterPaths(movie._id);

            if (fs.existsSync(paths.fullPath)) {
                console.log( '%s YES', movie._id );
                movie.poster_local = paths.dbPath;
                movie.save();
            } else {
                console.log( '%s NO', movie._id );
                movie.poster_local = undefined;
                movie.save();
            }
        });

        movieStream.on('error', function (err) {
            console.log("aww, received an error. all done.");
            console.log(err);
            done(null);
        });

        movieStream.on('close', function () {
            console.log("reached end of cursor. all done.");
            done(null);
        });

    });
};