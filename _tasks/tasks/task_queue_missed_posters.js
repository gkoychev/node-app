/**
 * User: gkoy
 * Date: 10.10.13
 * Time: 17:30
 */

var KueHelper = require("../../_phantom-crawler/helpers/kue-helper"),
    mongoose = require('mongoose'),
    mongooseInit = require('../../common/helpers/mongoose-init');


module.exports = function (config, limit, done) {

    var kueHelper = new KueHelper(config);
    var kue = kueHelper.getKue();
    var jobs = kue.createQueue();

    // Bootstrap db connection & models
    mongooseInit(config, {
        models_path: 'common/model',
        models: ['movie']

    }, function(){
        console.log('queue %d missed posters', limit);

        var Movie = mongoose.model('Movie');

        var q = Movie.find({poster_local: {$exists: false}}).select('id');
        if (limit)
            q = q.limit(limit);
        var movieStream = q.stream();

        movieStream.on('data', function (item) {
            kueHelper.addPosterParsingJob(jobs, item._id);
        });

        movieStream.on('error', function (err) {
            console.log("aww, received an error. all done.");
            console.log(err);
            done(err);
        });

        movieStream.on('close', function () {
            console.log("reached end of cursor. all done.");
            done(null);
        });

    });


};
