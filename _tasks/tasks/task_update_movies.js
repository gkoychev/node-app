/**
 * User: gkoy
 * Date: 10.10.13
 * Time: 17:30
 */

var KueHelper = require("../../_phantom-crawler/helpers/kue-helper"),
    mongoose = require('mongoose'),
    mongooseInit = require('../../common/helpers/mongoose-init');

module.exports = function (config, query, dry, done) {

    var kueHelper = new KueHelper(config);
    var jobs = kueHelper.getKue().createQueue();

    // Bootstrap db connection & models
    mongooseInit(config, {
        models_path: 'common/model',
        models: ['movie']

    }, function(){
        console.log('Update movies task');

        var Movie = mongoose.model('Movie');
        query = '('+query+')';
        var q = eval(query);

        if (dry) {
            //console.log(q);
            Movie.find(q).count( function(err, count){
                if (err)
                    return done(err);
                console.log('Movies to be updated: '+count);
                done(null);
            });
            return;
        }


        var q = Movie.find(q);
        var movieStream = q.stream();

        movieStream.on('data', function (movie) {
            kueHelper.addMovieParsingJob(jobs, movie._id);
        });

        movieStream.on('error', function (err) {
            console.log("aww, received an error. all done.");
            console.log(err);
            done(null);
        });

        movieStream.on('close', function () {
            console.log("reached end of cursor. all done.");
            done(null);
        });

    });

};
