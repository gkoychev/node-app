/**
 * User: gkoychev
 * Date: 16.04.13
 * Time: 18:33
 */
module.exports = function (config) {

    var kue = require('kue-mod');

    var express = require('express');

    var app = express();
    app.use(express.basicAuth('foo', 'bar'));
    app.use(kue.app);
    app.listen(config.kue_gui_port);
    kue.app.set('title', 'Job Queue: ('+config.app.name+')');

    /*
    kue.app.listen(config.kue_gui_port);
    kue.app.set('title', 'Job Queue: ('+config.app.name+')');
    */
    console.log('kue GUI is started on port ' + config.kue_gui_port);
};