/**
 * User: gkoychev
 * Date: 16.04.13
 * Time: 17:14
 */

// remove rate
// db.users.update({}, {$unset: {'rates.500':1} })
var async = require("async")
  , redis = require("redis")
  , kue = require('kue-mod');
kue.redis.createClient = function() {
    return redis.createClient();
};
var jobs = kue.createQueue();
var rClient = require("../common/helpers/redis-client");

var ScoreService = require('../common/services/movie-score-service');




var my_log = [];
function mylog_init(){
    my_log = [];
}
function mylog_get(){
    return my_log.join("\n");
}
function mylog(data, unformated){
    var text;
    if (typeof(data)=="string") {
        text = data;
    } else {
        if (unformated)
            text = JSON.stringify(data);
        else
            text = JSON.stringify(data, null, 4);
    }
    my_log.push(text);
    //console.log(data);
}



function clearCache(userId) {
    var key = 'cache:advice:' + userId;
    rClient.del(key);
}



module.exports = function (config) {

    var mongo = require('mongodb-wrapper');
    var db = mongo.db(config.mongo.connectionString);
    db.collection('users');
    db.collection('movies');
    db.collection('advices');
    db.on('error', function(err){
        console.log(err);
        return process.exit(config.consts.worker_fatal_exit_code);
    });

    jobs.process(config.consts.kue_job_type_calc,
        function(job, done) {
            console.log('I found a job: ' + job.id);
            console.log(job.data);

            //START WATERFALL

            async.waterfall([

                //( 1 ) get user rates
                function(callback) {
                    db.users.findOne({_id: mongo.ObjectID(job.data.userid)}, function(err, user) {
                        if (err || user==null) return callback(true);
                        return callback(null, user.rates || {});
                    });
                },

                //( 2 ) normalize rates
                function(rates, callback) {
                    var arr = Object.keys( rates ).map(function ( key ) { return rates[key]; });
                    var min = 0;//Math.min.apply( null, arr );
                    arr.push(9); //fix max not less than 9 (TODO: just to try)
                    var max = Math.max.apply( null, arr );


                    for(var id in rates) {
                        if (rates.hasOwnProperty(id)) {
                            rates[id] = (2*(rates[id] - min) / (max - min)) - 1;
                        }
                    }
                    mylog('NORMALIZE RATES: my rated movies');
                    mylog(["min/max", min, max], true);
                    //mylog(rates);
                    return callback(null, rates);
                },

                //( 3 ) collect user rates stats
                function(rates, callback) {
                    //collect array of ids
                    var ids = [];
                    for(var movieId in rates){
                        if (rates.hasOwnProperty(movieId)) {
                            ids.push(parseInt(movieId));
                        }
                    }
                    //iterate rated movies
                    db.movies.find({_id: {$in: ids}}).toArray(function(err, movies) {
                        if (err || movies==null) return callback(true);
                        var stats = ScoreService.CollectMovieRatingStats(movies, rates, mylog);
                        return callback(null, stats);
                    });
                },

                //( 4 ) iterate movies to calculate scores (ONE HEAVY OPERATION)
                function(stats, callback) {
                    console.log(stats);
                    //TODO: remove that after testing
                    db.users.update({_id: mongo.ObjectID(job.data.userid)}, { '$set': { 'stats': JSON.stringify(stats) }});

                    var cursor = db.movies.find();

                    cursor.getRawCursor(function(err, rawCursor){
                        if (err || !rawCursor) return callback(true);

                        var scores = [];
                        var r_min = false,
                            r_max = false;

                        rawCursor.each(function(err, movie){
                            if (movie==null) { //finish collection
                                console.log([r_min, r_max]);
                                return callback(null, scores);
                            }

                            if (movie.rating>0 && (r_min==false || movie.rating < r_min)) r_min = movie.rating;
                            if (r_max==false || movie.rating > r_max) r_max = movie.rating;

                            var score = ScoreService.CalculateMovieScore(movie, stats);

                            scores.push({id: movie._id, score: score});
                        });
                    });
                },

                //( 5 ) sort array
                function(scores, callback) {
                    scores.sort(function(a,b){
                        return b.score - a.score;
                    });
                    callback(null, scores);
                },

                //( 6 ) reduce (leave just ids)
                //TODO: leave 1000 +number of rated (so user will alwais have 1000 fresh advices)
                function(scores, callback) {
                    console.log(scores.slice(0, 5));
                    var ids = [];
                    for(var i=0; i<scores.length; i++)
                    {
                        ids.push(scores[i].id);
                    }
                    callback(null, ids.slice(0, config.consts.processor.advicesSerializeAmount));
                },

                //( 7 ) serialize
                function(ids, callback) {
                    //var serialized = ids.join(',');
                    //console.log('strlen: '+serialized.length/1024 + ' Kb');
                    db.advices.save({
                            _id: mongo.ObjectID(job.data.userid),
                            advice: ids
                        }, function(err, data){
                        //TODO: clear redis cache (will be nice to update it too)
                            if (!err)
                                clearCache(job.data.userid);
                            callback(err, data);
                        });
                }

            ], function(err, result){
                console.log('finish waterfall');
                console.log(arguments);
                //console.log(result.slice(0, 40)/*.length*/);

                //TODO: remove that after testing
                db.users.update({_id: mongo.ObjectID(job.data.userid)}, { '$set': { 'logs': mylog_get() }});
                mylog_init();
                clearCache(job.data.userid);
                //END _TODO

                return done();
            });
        });

    //remove completed jobs
    jobs.on('job complete', function(id){
        kue.Job.get(id, function(err, job){
            if (err) return;
            if (job.type == config.consts.kue_job_type_calc) {
                job.remove(function(err){
                    if (err)
                        console.log(arguments);
                    else
                        console.log('removed completed job #%d', job.id);
                });
            }
        });
    });
};
