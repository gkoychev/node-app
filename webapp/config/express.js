var express = require('express')
    , http = require('http')
    , path = require('path')
    , RedisStore = require('connect-redis')(express)   //session connector
    , redisClient = require('../../common/helpers/redis-client')
    , hbs_config = require('../config/hbs.js');


module.exports = function (app, config, passport) {

    var oneDay = 86400;

    // development only
    if (config.dev) {
        app.use(express.errorHandler());
        app.use(express.logger('dev'));
    }

    //configure handlebars templates
    hbs_config(app, config, 'webapp/views');

    app.use(express.favicon());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser(config.cookieSecret));
    app.use(express.session({
        store: new RedisStore({
            client: redisClient
        }),
        cookie: {  path: '/', httpOnly: true, maxAge: oneDay },    //day cookie
        secret: config.sessions.secret
    }));

    app.use(require('less-middleware')({ src: path.join(config.root, 'public') }));
    app.use(express.static(path.join(config.root, 'public')));

    // use passport session
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(function(req, res, next){
        res.locals.user = req.user;
        res.locals.authenticated = req.isAuthenticated();
        res.locals.app_title = config.app.name;
        next();
    });

    app.use(app.router);

};