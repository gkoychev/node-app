/**
 * User: gkoychev
 * Date: 04.04.13
 * Time: 19:33
 */
var TwitterStrategy = require('passport-twitter').Strategy
  , VKontakteStrategy = require('passport-vkontakte').Strategy
  , FacebookStrategy = require('passport-facebook').Strategy;

var mongoose = require('mongoose')
  , User = mongoose.model('User');

module.exports = function (passport, config) {

    // serialize sessions
    passport.serializeUser(function(user, done) {
        /*console.log("serializeUser: " + user.key);
        done(null, user.key);*/
        console.log("serializeUser: " + user.id);
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        console.log("deserializeUser: " + id);
        User.getUserById(id, function (err, user) {
            //console.log("deserialized");
            //console.log(user);
            done(err, user);
        });
    });


    function loginBySomeService(profile, done, fieldsCallback) {

        var q = {};
        q[profile.provider+'.id'] = profile.id;
        User.findOne(q, function (err, user) {
            if (err) { return done(err); }
            if (!user) {
                console.log("Create new user");
                user = new User({
                    name: profile.displayName
                });
                user.generateApiKey();
            }
            user[profile.provider] = profile._json;
            if (fieldsCallback) {
                fieldsCallback(user, profile);
            }
            user.save(function (err) {
                if (err) return done(err);
                return done(err, user)
            });
            //}
            /*else {
                return done(err, user);
            }*/
        });
    }

    function proposeLinkProfile(user, profile, done, fieldsCallback){
        user[profile.provider] = profile._json;
        if (fieldsCallback) {
            fieldsCallback(user, profile);
        }
        user.save(done);
    }

    // use twitter strategy
    passport.use(new TwitterStrategy({
            consumerKey: config.twitter.clientID,
            consumerSecret: config.twitter.clientSecret,
            callbackURL: config.twitter.callbackURL,
            passReqToCallback: true
        },
        function(req, token, tokenSecret, profile, done) {
            //console.log(profile);
            delete profile._json['status'];     //filter shit info
            if (req.isAuthenticated()) {
                console.log('already logged in');
                proposeLinkProfile(req.user, profile, done, twFieldsFilter);
            } else {
                loginBySomeService(profile, done, twFieldsFilter);
            }
        }
    ));

    passport.use(new FacebookStrategy({
            clientID:     config.facebook.clientID,
            clientSecret: config.facebook.clientSecret,
            callbackURL:  config.facebook.callbackURL,
            passReqToCallback: true,
            profileFields: ['id', 'username', 'displayName', 'photos', 'emails', 'profileUrl', 'gender' ]
        },
        function(req, token, tokenSecret, profile, done) {
            console.log(profile);
            if (req.isAuthenticated()) {
                console.log('already logged in');
                proposeLinkProfile(req.user, profile, done, fbFieldsFilter);
            } else {
                loginBySomeService(profile, done, fbFieldsFilter);
            }
        }
    ));

    passport.use(new VKontakteStrategy({
            clientID:     config.vkontakte.clientID,
            clientSecret: config.vkontakte.clientSecret,
            callbackURL:  config.vkontakte.callbackURL,
            passReqToCallback: true
        },
        function(req, token, tokenSecret, profile, done) {
            console.log(profile);
            if (req.isAuthenticated()) {
                console.log('already logged in');
                proposeLinkProfile(req.user, profile, done, vkFieldsFilter);
            } else {
                loginBySomeService(profile, done, vkFieldsFilter);
            }
        }
    ));


    function fbFieldsFilter(user, profile){
        user.gender = profile["gender"];

        if (!user.email && profile["emails"] && profile["emails"][0])
            user.email = profile["emails"][0].value;

        if (profile["photos"] && profile["photos"][0])
            user.avatar = profile["photos"][0].value;
    }

    function vkFieldsFilter(user, profile){
        if (profile._json.photo)
            user.avatar = profile._json.photo;
    }

    function twFieldsFilter(user, profile){
        console.log(profile["photos"]);
        if (profile["photos"] && profile["photos"][0])
            user.avatar = profile["photos"][0].value;
    }
};