var authRequired = require('../middlewares/auth-required');

module.exports = function (app, config, passport) {
    //routes
    var controllers = require('../controllers')
        , auth = require('../controllers/auth')
        , user = require('../controllers/user')
        , movie = require('../controllers/movie');

    app.get('/', controllers.index);



    // AUTH routes

    app.get('/auth/twitter', passport.authenticate('twitter', { failureRedirect: '/' }), auth.signin);
    app.get('/auth/twitter/callback', passport.authenticate('twitter', { failureRedirect: '/' }), auth.authCallback);

    app.get('/auth/facebook', passport.authenticate('facebook', { scope: [ 'email', 'user_about_me'], failureRedirect: '/' }), auth.signin);
    app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/' }), auth.authCallback);

    app.get('/auth/vkontakte', passport.authenticate('vkontakte', { failureRedirect: '/' }), auth.signin);
    app.get('/auth/vkontakte/callback', passport.authenticate('vkontakte', { failureRedirect: '/' }), auth.authCallback);

    app.get('/logout', auth.logout);

    // USER
    app.get('/profile', authRequired, user.index);
    app.get('/advice', authRequired, user.advicePage);
    app.post('/advice/get', authRequired, user.adviceAjax);

    // MOVIE
    app.get('/movie/:id', movie.index);
    app.post('/movie/:id/rate', authRequired, movie.rate);
    //app.get('/movie/:id/rate', authRequired, movie.rate);   //todo: remove later
};
