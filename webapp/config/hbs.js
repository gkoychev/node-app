/**
 * User: gkoi
 * Date: 10.06.13
 * Time: 12:11
 */
var exphbs  = require('express3-handlebars'),
    path = require('path'),
    http = require('http');

module.exports = function (app, config, views_base_path) {

    // base views path
    var views_path = path.join(config.root, views_base_path);

    app.set('views', views_path + '/pages');
    app.engine('hbs', exphbs({
        layoutsDir: views_path,
        partialsDir: views_path + '/partials',
        helpers: {
            json: function(context) {
                return JSON.stringify(context, null, 4);
            },
            'join': function(items, block) {
                var delimiter = block.hash.delimiter || ", ",
                    start = start = block.hash.start || 0,
                    len = items ? items.length : 0,
                    end = block.hash.end || len;

                if(end > len) end = len;

                if ('function' === typeof block.fn) {
                    return items.slice(start, end).map(function(item) {
                        return block.fn(item);
                    }).join(delimiter);
                } else {
                    return [].concat(items).slice(start, end).join(delimiter);
                }
            }
        },
        extname: '.hbs',
        defaultLayout: 'layout'
    }));
    app.set('view engine', 'hbs');


    if (config.dev !== true)
        app.enable('view cache');


    //extend response object
    var response = http.ServerResponse.prototype;

    response.renderPage = function(page, options, config, callback) {
        if (options == undefined)
            options = {};

        if (config==undefined)
            config = {section: page};
        else
            config.section = page;

        options.config = config;

        this.render(page, options, callback);
    };
};

