
exports.authCallback = function (req, res) {
    res.redirect('/');
};

exports.signin = function (req, res) {

};

exports.logout = function (req, res) {
    req.logout();
    res.redirect('/');
};
