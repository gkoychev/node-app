/**
 * User: gkoychev
 * Date: 30.04.13
 * Time: 19:11
 */

var ApiClient = require('../libs/api-client');


function sortObject(obj) {
    var arr = [];
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop]
            });
        }
    }
    arr.sort(function(a, b) { return b.value - a.value; });
    return arr; // returns array
}

/*
 * GET profile page.
 */
exports.index = function(req, res){
    var api_client = new ApiClient(req.user.getToken());
    api_client.getMe(function(err, profile){

        console.log(arguments);

        var genres =[],
            actors=[],
            directors = [],
            countries = [];

        var logs = (profile['logs'] || "").replace("\\n", "\n");
        console.log(logs);

        if (profile.stats) {
            var stats = JSON.parse(profile.stats);
            genres = sortObject(stats.genres);
            actors = sortObject(stats.actors);
            directors = sortObject(stats.directors);
            countries = sortObject(stats.countries);
        }
        delete profile.stats;
        delete profile.rates;
        delete profile.logs;


        function mapValues(el){
            el.value = Math.round(el.value * 1000)/1000;
            return el;
        }

        res.renderPage('profile', {
            profile: profile,
            logs: logs,
            genres: genres.map(mapValues),
            actors: actors.map(mapValues),
            directors: directors.map(mapValues),
            countries: countries.map(mapValues)
        });
    });
};

exports.advicePage = function(req, res){
    res.renderPage('advice', { data: false });
};

exports.adviceAjax = function(req, res){
    var start = req.param('start') || 0;
    var take = req.param('take') || 10;

    var api_client = new ApiClient(req.user.getToken());
    api_client.getAdvice(function(err, data){
        res.json(data);
    },{start: start, take: take});
};
