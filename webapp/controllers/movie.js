/**
 * User: gkoychev
 * Date: 07.05.13
 * Time: 18:47
 */
var ApiClient = require('../libs/api-client');

/*
 * GET movie info
 */
//TODO: here to refactor!!!!!!!!

exports.index = function(req, res){

    var id = req.param('id');
    //console.log(req.user.getToken);
    var api_client = new ApiClient(req.user ? req.user.getToken() : null);
    api_client.getMovie(id, function(err, data){
        if (err)
            return res.send(data.code, data.text);

        var movie = data;
        //TODO: temp for testing
        var debug = [];
        if (req.user && req.user.stats) {
            var ScoreService = require('../../common/services/movie-score-service');
            var stats = JSON.parse(req.user.stats);
            ScoreService.CalculateMovieScore(movie, stats, function(text){
                debug.push(text);
            });
        }


        res.renderPage('movie', { movie: movie, debug: debug.join("\n")  });
    });
};

exports.rate = function(req, res){
    var api_client = new ApiClient(req.user.getToken());

    console.log(req.param('rating'));

    api_client.rateMovie(req.param('id'), req.param('rating'), function(err, data){
        return res.json(200, data);
    });
}