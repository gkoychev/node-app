/**
 * User: rarog
 * Date: 30.04.13
 * Time: 22:39
 */
var sys = require('sys');
var http = require('http');
var url = require('url');
var util = require('util');
var log4js = require('log4js');
var logger = log4js.getLogger("API Client");
var querystring = require('querystring');

var config = require('../../config');

module.exports = function (api_key) {

    /**
     * Returns profile info (just demonstration of API client)
     */
    this.getMe = function(callback, params) {
        var url = '/api/me';
        sendMessage(url, 'GET', paramsToQueryString(params), callback);
    };

    /**
     * Returns recommendations
     */
    this.getAdvice = function(callback, params) {
        var url = '/api/advice';
        sendMessage(url, 'GET', paramsToQueryString(params), callback);
    };

    /**
     * Returns movie info
     */
    this.getMovie = function(id, callback, params) {
        var url = util.format('/api/movie/%d', id);
        sendMessage(url, 'GET', paramsToQueryString(params), callback);
    };

    this.rateMovie = function(id, rate, callback, params) {
        var url = util.format('/api/movie/%d/rate/%d', id, rate);
        sendMessage(url, 'GET', paramsToQueryString(params), callback);
    };



    /**
     * Call the JSON API with the provided method and query parameters.  Call the callback function once the
     * request is completed, passing in the JSON object returned from the server or null in case of error.
     */
    function sendMessage(api_url, method, query, callback) {

        var parsedUrl = url.parse(api_url);

        var host = parsedUrl.host == null ? config.api.api_connect_host : parsedUrl.host;
        var port = parsedUrl.port == null ? config.api.port : parsedUrl.port;
        var path =  (parsedUrl.pathname + ((query === undefined) ? '' : ('?' + query)));

        logger.info('api: %s %s %s', method, api_url, query);

        var request = http.request({
            'host': host,
            'port': port,
            'path': path,
            'method': method

        }, function(response) {
            logger.info('STATUS: %s', response.statusCode);

            response.setEncoding('utf8');
            var responseBody = '';
            response.on('data', function (chunk) {
                responseBody += chunk;
            });
            response.on('end', function () {
                try {
                    var data = JSON.parse(responseBody);
                    if (response.statusCode > 399) {
                        logger.error('Got error response code %s, request failed.', response.statusCode);
                        callback(true, {code:response.statusCode, text:data.message || ""});
                    }
                    else
                        callback(null, data);
                } catch(e) {
                    logger.error('Failed to parse JSON response:\n%s', e.stack);
                    callback(true, {code:500, text:"Request Failed"});
                }
            });
        });

        request.on('error', function(err) {
            logger.error('Failed to send request:\n%s', sys.inspect(err));
            callback(true, {code:500, body:"Request Failed"});
        });

        request.end();
    }

    /**
     * Turn a hash of parameters into a query string
     */
    function paramsToQueryString(params) {
        if (params == null) params = {};
        if (api_key)
            params['api_key'] = api_key;
        return querystring.stringify(params);
    }

};