module.exports = function (config) {

    var express = require('express')
        , mongooseInit = require('../common/helpers/mongoose-init')
        , passport = require('passport');

    // Bootstrap db connection & models
    mongooseInit(config, {
        models_path: 'common/model',
        models: ['user']
    });

    // setup express
    var app = express();

    require('./config/passport')(passport, config);

    require('./config/express')(app, config, passport);
    require('./config/routes')(app, config, passport);

    //run app
    app.listen(config.app.port);

    console.log('Frontend server listening on port ' + config.app.port);
};