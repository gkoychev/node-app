/**
 * User: gkoychev
 * Date: 30.04.13
 * Time: 19:21
 */

module.exports = function (req, res, next) {
    if (!req.isAuthenticated()) {
        //return res.send(403, 'auth required');
        return res.redirect('/');
    }
    return next();
};