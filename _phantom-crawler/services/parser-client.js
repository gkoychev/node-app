/**
 * User: rarog
 * Date: 30.04.13
 * Time: 22:39
 */
var sys = require('sys');
var http = require('http');
var url = require('url');
var util = require('util');
var log4js = require('log4js');
var logger = log4js.getLogger("Parser Client");
var querystring = require('querystring');


module.exports = function (config) {

    /**
     * Returns profile info (just demonstration of API client)
     */
    this.parseMovie = function(id, callback) {
        var url = config.crawler.parse_movie_url;
        sendMessage(url, 'GET', {id: id}, callback);
    };

    this.parseLinks = function(linksUrl, callback) {
        var url = config.crawler.parse_links_url;
        sendMessage(url, 'GET', {url: linksUrl}, callback);
    };

    this.getAllLinks = function(linksUrl, callback) {
        var url = config.crawler.parse_all_links_url;
        sendMessage(url, 'GET', {url: linksUrl}, callback);
    };



    /**
     * Call the JSON API with the provided method and query parameters.  Call the callback function once the
     * request is completed, passing in the JSON object returned from the server or null in case of error.
     */
    function sendMessage(api_url, method, params, callback) {

        var query = querystring.stringify(params);
        var parsedUrl = url.parse(api_url);

        var host = parsedUrl.host == null ? config.crawler.phantom_host : parsedUrl.host;
        var port = parsedUrl.port == null ? config.crawler.phantom_port : parsedUrl.port;
        var path =  (parsedUrl.pathname + ((query === undefined) ? '' : ('?' + query)));

        logger.info('api: %s %s %s', method, api_url, query);

        var request = http.request({
            'host': host,
            'port': port,
            'path': path,
            'method': method

        }, function(response) {
            logger.info('STATUS: %s', response.statusCode);

            response.setEncoding('utf8');
            var responseBody = '';
            response.on('data', function (chunk) {
                responseBody += chunk;
            });
            response.on('end', function () {
                try {
                    var data = JSON.parse(responseBody);
                    if (response.statusCode > 399) {
                        logger.error('Got error response code %s, request failed.', response.statusCode);
                        callback(true, response.statusCode);
                    }
                    else
                        callback(null, data);
                } catch(e) {
                    logger.error('Failed to parse JSON response:\n%s', e.stack);
                    callback(true, 500);
                }
            });
        });

        request.on('error', function(err) {
            logger.error('Failed to send request:\n%s', sys.inspect(err));
            callback(true, 500);
        });

        request.end();
    }


};