/**
 * User: rarog
 * Date: 22.08.13
 * Time: 18:51
 */
var async = require('async'),
    Processor = require("./movie-processor");

module.exports = function(config, jobs, kueHelper) {

    var processor = Processor(config, jobs, kueHelper);

    return {
        //PROCESS MOVIES
        movieProcessingJob: function(job, done){
            console.log('Crawler kue! I found a "movie" job! Movie: ' + job.data.movie);
            //console.log(job.data);

            var pause = config.crawler.kue_movie_job_pause
                + Math.floor((Math.random()*2*config.crawler.kue_movie_job_pause_threshold) - config.crawler.kue_movie_job_pause_threshold);

            job.log('waiting ' + pause + 's');
            setTimeout(function(){
                job.log('processing...');
                console.log('Crawler kue! START JOB!');

                var movieID = job.data.movie;

                //START WATERFALL
                async.waterfall([
                    function(done){
                        done(null, movieID); //start params
                    },
                    processor.ParseMovieData,   //parse data

                    processor.FindNewMovieIds,  //test what movie is missed
                    processor.QueueNewMovies,    //insert missed movies to kue

                    processor.CheckMovieData,   //test data quality (skip porn & unfilled films)
                    processor.SaveMovieData,    //update movie in database

                    //processor.UpdateMoviePosterFromWeb
                    //TODO: not return error if nothing to load (to avoid retry)
                    processor.GetPosterUrl,
                    processor.DownloadPoster,
                    processor.SavePosterPath


                ],
                    function(err, result){
                        console.log('Finish crawler waterfall');
                        console.log(arguments);

                        //TODO: if not 404 than insert failed id into end of the Queue
                        if (result==408) {      //retry
                            console.log("ADD movie: "+movieID+" to the que");
                            kueHelper.addMovieParsingJob(jobs, movieID);

                            //TODO: temp to leave self on long time
                            return process.exit(config.consts.worker_super_fatal_exit_code);
                        }

                        return done();
                    }
                );
            }, pause*1000);
        },


        //PROCESS LINKS
        linksProcessingJob: function(job, done){
            console.log('Crawler kue! I found a "links" job! URL: '+job.data.url);
            //console.log(job.data);

            var pause = config.crawler.kue_links_job_pause
                + Math.floor((Math.random()*2*config.crawler.kue_links_job_pause_threshold) - config.crawler.kue_links_job_pause_threshold);

            job.log('waiting ' + pause + 's');
            setTimeout(function(){
                job.log('processing...');
                console.log('Crawler kue! START JOB!');

                if (job.data.level) {
                    //console.log('??????????????????? DEEP: ' + job.data.level);
                    processor.ParseMovieLinksDeep(job.data.url, function(err, data){
                        //console.log('??????????????????? DEEP Url count: ' + data.length);
                        for(var i=0; i<data.length; i++) {
                            kueHelper.addMovieLinksParsingJob(jobs, "http://www.kinopoisk.ru" + data[i]);
                        }
                        return done();
                    });
                    return;
                }

                //WATERFALL
                var parseUrl = job.data.url;

                var waterfall_tasks = [
                    function(cb){
                        cb(null, parseUrl); //start params
                    },
                    processor.ParseMovieLinksData,   //parse data

                    processor.FindNewMovieIds,  //test what movie is missed
                    processor.QueueNewMovies    //insert missed movies to kue
                ];
                if (job.data.force) {
                    //delete waterfall_tasks
                    console.log('FORCE PARSE ALL MOVIES');
                    waterfall_tasks.splice(2,1);
                    console.log(waterfall_tasks);
                }

                async.waterfall(waterfall_tasks,
                    function(err, result){
                        console.log('Finish crawler waterfall for link');
                        //console.log(arguments);

                        //TODO: if not 404 than insert failed id into end of the Queue
                        if (result==408) {      //retry
                            console.log("ADD link: " + parseUrl + " to the que");
                            kueHelper.addMovieLinksParsingJob(jobs, parseUrl);
                        }

                        return done();
                    }
                );
            }, pause*1000);
        },


        //PROCESS POSTERS
        posterProcessingJob: function(job, done){
            var movieID = job.data.movie;
            console.log('Crawler kue! I found a "POSTER" job! Movie: ' + movieID);

            var pause = config.crawler.kue_poster_job_pause
                + Math.floor((Math.random()*2*config.crawler.kue_poster_job_pause_threshold) - config.crawler.kue_poster_job_pause_threshold);

            job.log('waiting ' + pause + 's');
            setTimeout(function(){
                job.log('processing...');
                console.log('Crawler kue! START JOB!');

                //START WATERFALL
                async.waterfall([
                    function(done){
                        done(null, movieID); //start params
                    },
                    processor.GetPosterUrl,
                    processor.DownloadPoster,
                    processor.SavePosterPath

                    //TODO: run some new tasks
                ],
                    function(err, result){
                        console.log('Finish crawler waterfall');
                        if (err)
                            console.log(arguments);

                        return done();
                    }
                );
            }, pause);

        }
    };
};