/**
 * User: gkoy
 * Date: 07.08.13
 * Time: 17:10
 */

var Parser = require('./parser-client'),
    Validator = require('./movie-validator'),
    PathService = require('../../common/services/path-service');

var path = require('path'),
    child_process = require('child_process'),
    async = require('async');


require('../../common/helpers/my-utils');

var mongoose = require('mongoose'),
    mongooseInit = require('../../common/helpers/mongoose-init'),
    redis = require("redis");


var download_image = function(url, dir, file, done) {

    // compose the wget command
    var dest = path.join(dir, file);
    var wget = 'mkdir -p ' + dir + ' && wget -x -O ' + dest + ' ' + url;

    // excute wget using child_process' exec function
    child_process.exec(wget, function(err, stdout, stderr) {
        if (err) return done(err);
        else {
            console.log('Image downloaded to : ' + dest);
            child_process.exec('file '+dest, function(err, stdout){
                if (err) return done(err);
                //console.log(stdout);

                if (stdout.match(/(image data|Minix)/i)){
                    done(null);
                } else {
                    done(true);
                }
            });
        }
    });
};



module.exports = function(config, kueJobs, kueHelper){

    var parser = new Parser(config);
    var pathService = new PathService(config);

    // Bootstrap db connection & models
    mongooseInit(config, {
        models_path: 'common/model',
        models: ['movie']
    });
    var Movie = mongoose.model('Movie');

    var notValidMovies = {};

    var client = redis.createClient();
    client.get(config.crawler.redis_not_valid_key, function(err ,res){
        if (res!=null) {
            notValidMovies = JSON.parse(res);
            console.log('notValidMovies count: ' + Object.keys(notValidMovies).length);
        }
    });


    return {
        ParseMovieLinksDeep: function(url, done){
            parser.getAllLinks(url, function(err ,data){
                if (err)
                    return done(err, data);
                else {

                    return done(null, data);
                }
            });
        },

        ParseMovieLinksData: function(url, done){
            parser.parseLinks(url, function(err ,data){
                if (err)
                    return done(err, data);
                else
                    return done(null, {ids:data});
            });
        },

        ParseMovieData: function(id, done){
            parser.parseMovie(id, function(err ,data){
                if (err)
                    return done(err, data);
                else
                    return done(null, data);
            });
        },

        FindNewMovieIds: function(movieData, done){
            Movie.find()
                .where('_id').in(movieData.ids)
                .select('_id')
                .exec(function(err, movies) {
                    if (err)
                        return done(true, 500, "mongo fail");

                    var old_ids = movies.map(function(v){
                        return ""+v._id;
                    });

                    //filter out old_ids
                    movieData.ids = movieData.ids.diff(old_ids);

                    //filer out "not valid movies"
                    movieData.ids = movieData.ids.filter(function(el) {
                        return !(notValidMovies[el]===true);
                    });

                    console.log("new: "+JSON.stringify(movieData.ids, null, 4));
                    console.log("not valid count: "+Object.keys(notValidMovies).length);
                    done(null, movieData);
                });
        },

        QueueNewMovies: function(movieData, done){
            //console.log(movieData.ids);
            for(var i=0; i<movieData.ids.length; i++) {
                kueHelper.addMovieParsingJob(kueJobs, movieData.ids[i]);
            }
            done(null, movieData);
        },

        CheckMovieData: function(movieData, done){
            if (Validator.validate(movieData['movie'])){
                delete notValidMovies[movieData['movie']._id];  //remove id from not valid
                done(null, movieData);
            } else{
                notValidMovies[movieData['movie']._id] = true;

                done(true, 400, "validation failed: bad movie");

                //save notValidMovies
                client.set(config.crawler.redis_not_valid_key, JSON.stringify(notValidMovies));

                //remove that movie from DB
                Movie.remove({_id: movieData['movie']._id}, function(){
                    //console.log('removed: '+movieData['movie']._id);
                });
            }
        },

        SaveMovieData: function(movieData, done){
            console.log('> SaveMovieData');
            movieData['movie'].updated = new Date().getTime();
            var id = movieData['movie']._id;
            delete movieData['movie']._id;

            Movie.update({_id: id}, movieData['movie'], {upsert: true}, function (err){
                if (err) return done(true, 500, "save failed");
                done(null, id);
            });
        },


        ///methods for posters
        GetPosterUrl: function(movieId, done){
            Movie.findOne({_id: movieId}, function(err, data){
                if (err) return done(true);

                if (!data['poster'])
                    done(true, 400, "movie have no poster");

                done(null, {id: movieId, url: data['poster']});
            });
        },

        DownloadPoster: function(data, done){
            var paths = pathService.getMoviePosterPaths(data.id);
            //console.log(paths);

            download_image(data.url, paths.savePath, paths.fileName, function(err){
                if (err)
                    return done(true, "retry");

                data.dbPath = paths.dbPath;
                return done(null, data);
            });
        },

        SavePosterPath: function(data, done){
            Movie.update({_id: data.id}, {$set: {poster_local: data.dbPath}}, function(err, res){
                if (err) return done(err, res);

                return done(null);
            });
        },

        /*
        FindNextPosterToLoad: function(done){
            Movie.find({poster_local: {$exists: false}}).select('id').limit(1).exec(function(err, res){
                if (err) return done(err, res);

                for (var i=0; i<res.length; i++) {
                    //console.log(res[i]._id);
                    kueHelper.addPosterParsingJob(kueJobs, res[i]._id);
                }
                done(null);
            });
        },*/

        UpdateMoviePosterFromWeb: function(movieID, done) {
            var that = this;
            console.log('UpdateMoviePosterFromWeb: %s', movieID);
            console.log(arguments);
            require('async').waterfall([
                function(cb){
                    console.log('start');
                    cb(null, movieID); //start params
                },
                that.GetPosterUrl,
                that.DownloadPoster,
                that.SavePosterPath
            ],
                function(err, result){
                    console.log('Finish UpdateMoviePosterFromWeb');
                    console.log(arguments);
                    if (err)
                        console.log(arguments);

                    done();
                }
            );


        }
    }
};


