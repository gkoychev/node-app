/**
 * User: gkoy
 * Date: 08.08.13
 * Time: 16:25
 */

var myEnv = require('schema')(
    'myEnvironment', {
        locale: 'ru'
    }
);

var movieSchema = myEnv.Schema.create({
    type: 'object',
    properties: {
        title: {
            type: 'string',
            minLength: 1,
            maxLength: 150,
            fallbacks: {
                maxLength: 'truncateToMaxLength'
            }
        },
        year: {
            type: 'integer',
            minimum: 1800,
            maximum: 9999
        },
        "poster":{
            type: "string",
            minLength: 10,
            pattern: ".*\\.jpg$"
        },
        "genres": {
            "type": "array",
            "minItems": 1,
            "items": {
                "type": "string",
                "minLength": 4
            },
            "uniqueItems": true
        },
        "actors": {
            "type": "array",
            "minItems": 2,
            "items": {
                "type": "string",
                "minLength": 2
            },
            "uniqueItems": true
        }
    },
    additionalProperties: true
});



module.exports.validate = function(data){

    var validation = movieSchema.validate(data);
    if (validation.isError()) {
        console.log(JSON.stringify(validation.getError()));
        return false;
    }
    if (data['genres'].indexOf("для взрослых")!=-1){
        console.log("для взрослых");
        return false;
    }
    return true;
};



