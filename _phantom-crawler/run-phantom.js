/**
 * User: gkoy
 * Date: 19.07.13
 * Time: 15:42
 */
var runPhantom = require('./helpers/run-phantom'),
    path = require('path');

module.exports = function (config) {

    var phantom_proc = false;

    //start phantom
    runPhantom({
        port: config.crawler.phantom_port,
        phantomScript: path.join(__dirname, './_phantom-scripts/parsing-server.js')
    }, function(err, phantom){
        if (err)
            return console.error('pantom is failed to start');
        console.log('Phantom is started on port: ' + config.crawler.phantom_port);
        phantom_proc = phantom;
    });

    //kill phantom
    var killPhantom = function() {
        console.log(arguments);
        if (phantom_proc) {
            console.log('killing phantomjs: '+ phantom_proc.pid);
            process.kill(phantom_proc.pid);
        }
    };

    //process.on("uncaughtException", killPhantom);
    //process.on("SIGINT", killPhantom);
    //process.on("SIGTERM", killPhantom);
    process.on("exit", killPhantom);

};

