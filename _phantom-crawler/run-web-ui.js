/**
 * User: gkoy
 * Date: 22.07.13
 * Time: 18:09
 */
var express = require('express');
var exphbs  = require('express3-handlebars'),
    path = require('path');

module.exports = function (config) {

    var KueHelper = require("./helpers/kue-helper");
    var kueHelper = new KueHelper(config);

    var kue = kueHelper.getKue();
    var jobs = kue.createQueue();



    var app = express();
    app.use(express.bodyParser());

    // base views path
    var views_path = path.join(config.root, '_phantom-crawler/views');

    app.use(require('less-middleware')({ src: path.join(config.root, '_phantom-crawler', 'public') }));
    app.use(express.static(path.join(config.root, '_phantom-crawler', 'public')));

    app.set('views', views_path);
    app.engine('hbs', exphbs({
        layoutsDir: views_path,
        partialsDir: views_path + '/partials',
        helpers: {
            json: function(context) {
                return JSON.stringify(context, null, 4);
            }
        },
        extname: '.hbs',
        defaultLayout: 'layout'
    }));
    app.set('view engine', 'hbs');

    //auth
    app.use(express.basicAuth('admin', 'admin'));


    app.get('/', function(req, res){
        //res.send('Hello World');
        res.render('index', {app_title: 'Crawler Admin'});
    });

    app.post('/parse-movie', function(req, res){
        var id = req.body.id;
        if (id.match(/^\d+$/)){
            kueHelper.addMovieParsingJob(jobs, id);
            res.json({error:true, message:"ok"});
        } else {
            res.json({error:true, message:"bad movie id"});
        }
    });

    app.post('/parse-links', function(req, res){
        var url = req.body.url;
        if (url.match(/^https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}$/)){
            kueHelper.addMovieLinksParsingJob(jobs, url, req.body.force);
            res.json({error:true, message:"ok"});
        } else {
            res.json({error:true, message:"bad url"});
        }
    });

    app.post('/parse-links-2', function(req, res){
        var url = req.body.url;
        if (url.match(/^https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}$/)){
            kueHelper.addMovieLinksParsingJob(jobs, url, req.body.force, 2);
            res.json({error:true, message:"ok"});
        } else {
            res.json({error:true, message:"bad url"});
        }
    });


    app.listen(config.crawler.web_admin_port);
    console.log('Crawler web admin listening on port: ' + config.crawler.web_admin_port);

};