/**
 * User: gkoy
 * Date: 22.07.13
 * Time: 15:08
 */
var async = require("async"),
    KueHelper = require("./helpers/kue-helper"),
    MyKueJobs = require("./services/my-kue-jobs");


module.exports = function (config) {

    var kueHelper = new KueHelper(config);

    var kue = kueHelper.getKue();
    var jobs = kue.createQueue();


    var myKueJobs = new MyKueJobs(config, jobs, kueHelper);

    process.on('SIGINT', function() {
        console.log('pressed Ctr+C.');
        jobs.active(function(err, ids) {
            if (err) {
                console.log('some get active jobs list err:');
                console.log(err);
                return;
            }
            console.log(ids);
            for(var i=0; i < ids.length; i++) {
                kue.Job.get(ids[i], function(err, job) {
                    if (err) {
                        console.log('some get job err:');
                        console.log(err);
                        return;
                    }
                    //console.log(job);
                    //console.log(job.data);
                    var movieID = job.data.movie;
                    console.log('set movie ' + movieID + ' to inactive.');
                    job.state('inactive').save();
                });
            }
        });
        setTimeout(function(){
            process.exit();
        },4000);
    });


    //test
    //kueHelper.addMovieParsingJob(jobs, 999);
    //kueHelper.addPosterParsingJob(jobs, 777);
    //kueHelper.addMovieLinksParsingJob(jobs, "http://www.kinopoisk.ru/popular/");

    //PROCESSING
    jobs.process(config.crawler.kue_movie_job,
        myKueJobs.movieProcessingJob);

    jobs.process(config.crawler.kue_movie_links_job,
        myKueJobs.linksProcessingJob);

    jobs.process(config.crawler.kue_poster_job,
        myKueJobs.posterProcessingJob);


    //remove completed jobs
    jobs.on('job complete', function(id){
        kue.Job.get(id, function(err, job){
            if (err) return;
            if (job.type == config.crawler.kue_movie_job ||
                job.type == config.crawler.kue_movie_links_job ||
                job.type == config.crawler.kue_poster_job) {
                job.remove(function(err){
                    if (err)
                        console.log(arguments);
                    else
                        console.log('removed completed job #%d', job.id);
                });
            }
        });
    });


    if (config.crawler.kue_ui_active) {
        //run KUE UI
        kue.app.listen(config.crawler.kue_ui_port);
        kue.app.set('title', 'Crawler Queue');

        console.log('Crawler Queue GUI is started on port ' + config.crawler.kue_ui_port);
    }
};


