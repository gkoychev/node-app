/**
 * User: gkoy
 * Date: 01.10.13
 * Time: 17:33
 */

module.exports = {
    phantom_host: 'localhost',
    phantom_port: 8083,
    kue_ui_active: true,
    kue_ui_port: 5000,
    web_admin_port: 8084,

    kue_redis_db: 2,
    redis_not_valid_key: 'notValidMovies',

    kue_movie_job: 'movie',
    kue_movie_job_pause: 10,
    kue_movie_job_pause_threshold: 2,
    kue_movie_links_job: 'movie_links',
    kue_links_job_pause: 10,
    kue_links_job_pause_threshold: 2,

    kue_poster_job: 'poster',
    kue_poster_job_pause: 250,  //in ms
    kue_poster_job_pause_threshold: 0,

    parse_movie_url: '/parse-movie',
    parse_links_url: '/collect-links',
    parse_all_links_url: '/collect-all-links'

};
