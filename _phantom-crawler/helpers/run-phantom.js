/**
 * User: gkoy
 * Date: 18.07.13
 * Time: 17:15
 */
var child = require('child_process');
var path = require('path');

module.exports = function(options, callback) {

    if(options === undefined) options={};
    if(options.phantomPath === undefined) options.phantomPath = 'phantomjs';
    if(options.phantomScript === undefined) options.phantomScript = 'run.js';

    function spawnPhantom(port, done){
        var args = [];
        args = args.concat([options.phantomScript, port]);

        var phantom = child.spawn(options.phantomPath, args);
        phantom.stdout.on('data',function(data){
            //return console.log('phantom stdout: '+data);
        });
        phantom.stderr.on('data',function(data){
            return console.warn('phantom stderr: '+data);
        });
        var exitCode=0;
        phantom.on('error',function(){
            exitCode = -1;
        });
        phantom.on('exit',function(code){
            exitCode = code;
        });
        setTimeout(function(){ //wait a bit to see if the spawning of phantomjs immediately fails due to bad path or similar
            done(exitCode!==0, phantom);
        }, 100);
    }

    //it is passed to done
    function afterPhantomExecution(err, phantom){
        if(err)
            return callback(err);
        phantom.on('exit', function(code, signal){
            if(code !== 0 && signal === null){
                console.warn('phantom crash: code '+code + ', signal='+signal);
            }
            console.log('respawn phantomjs... ');
            spawnPhantom(options.port, afterPhantomExecution);
        });

        return callback(null, phantom);
    }

    spawnPhantom(options.port, afterPhantomExecution);

};
