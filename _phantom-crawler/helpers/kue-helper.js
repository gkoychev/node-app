/**
 * User: gkoy
 * Date: 22.07.13
 * Time: 16:08
 */
var redis = require("redis"),
    kue = require('kue-mod');
    //config = require('kue-mod');


module.exports = function(config) {

    var db = config.crawler.kue_redis_db;

    return {
        getKue: function(){
            kue.redis.createClient = function() {
                var client = redis.createClient();
                client.select(db);
                return client;
            };
            return kue;
        },

        addMovieParsingJob: function(jobs, movieId){
            jobs.create(config.crawler.kue_movie_job, {
                title: "parse movie: " + movieId,
                uid: 'movie:' + movieId,
                movie: movieId
            }).priority(new Date().getTime()).save();
        },

        addMovieLinksParsingJob: function(jobs, url, force, level){
            jobs.create(config.crawler.kue_movie_links_job, {
                title: "parse Url: " + url,
                uid: 'url:' + url,
                url: url,
                force: force || false,
                level: level || false
            }).priority(new Date().getTime()).save();
        },

        addPosterParsingJob: function(jobs, movieId){
            jobs.create(config.crawler.kue_poster_job, {
                title: "get poster for movie: " + movieId,
                uid: 'poster:' + movieId,
                movie: movieId
            }).priority(new Date().getTime()).save();
        }

    }

};

