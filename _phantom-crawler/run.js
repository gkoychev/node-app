/**
 * User: gkoy
 * Date: 22.07.13
 * Time: 15:28
 */
var config = require('../config');
var crawler_config = require('./crawler-config');
config.crawler = crawler_config;

require('../common/helpers/cluster-node')(config, {
    workers: {
        phantom: {
            workerSrc: './_phantom-crawler/run-phantom'
        },
        kue_processor: {
            workerSrc: './_phantom-crawler/run-kue'
        },
        web_ui: {
            workerSrc: './_phantom-crawler/run-web-ui'
        }
    },
    autoRestart: true
});

