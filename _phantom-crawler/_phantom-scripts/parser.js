/**
 * User: rarog
 * Date: 11.07.13
 * Time: 20:53
 */

var utils = require('./libs/utils');

function ExtractFilmIds()
{
    //other links
    var other_films = [];
    var other_films_hash = {};
    $('a[href^="/film/"]').each(function(el){
        var url = $(this).attr('href');
        var m = url.match(new RegExp("/film/(\\d+)/$"));
        if (m) {
            if (!other_films_hash[m[1]]) {
                other_films.push(m[1]);
                other_films_hash[m[1]] = true;
            }
        }
    });
    return other_films;
}

function ExtractAllUrls(){
    var urls = [];
    var urls_hash = {};

    $('#content_block a[href]').each(function(i,e){
        if ($(this).hasClass('headerLink'))
            return;
        var url=$(this).attr('href');
        if(url && url[0]=="/" && url[1]!="/") {
            //if (url.match(/film\/\d+\//))
            //    return;
            if (url.match(/#/))
                return;
            if (url.match(/popular\/\.$/))
                return;

            if (!urls_hash[url]) {
                urls.push(url);
                urls_hash[url] = true;
            }
        }
    });
    return urls;
}

function ExtractData(movieId){

    function _x(STR_XPATH) {
        var xresult = document.evaluate(STR_XPATH, document, null, XPathResult.ANY_TYPE, null);
        var xnodes = [];
        var xres;
        while (xres = xresult.iterateNext()) {
            xnodes.push(xres);
        }
        return xnodes;
    }

    function parseListOfLinks(list){
        var ret = [];
        for (var i=0; i<list.length; i++){
            var link = $(list[i]);
            if (!link.hasClass('keywordLink') && $.trim(link.text())!="...")
            {
                ret.push( $.trim( link.text() ) );
            }
        }
        return ret;
    }

    //series params
    var serie = $("#headerFilm .moviename-big > span").text().indexOf("сериал") > -1;
    //find end year for series  2004 – 2011
    var match = $("#headerFilm .moviename-big > span").text().match(/\d\d\d\d\s+–\s+(\d\d\d\d)/i);
    var year_end = match ? match[1] : undefined;
    //year_end = $("#headerFilm .moviename-big").text();


    //var poster  = $('.popupBigImage > img[itemprop="image"]').attr('src');
    var poster  = $('img[itemprop="image"]').attr('src');
    //var title   = document.title;
    var title   = $('.moviename-big[itemprop=name]').children().remove().end().text();
    var title2  = $('span[itemprop=alternativeHeadline]').text();

    var year        = $.trim($(_x('//div[@id="infoTable"]/table/tbody/tr[1]/td[2]/div/a[1]')).text());
    var slogan      = $.trim($(_x('//*[@id="infoTable"]/table/tbody/tr[3]/td[2]')).text());

    var countries   = parseListOfLinks(_x('//*[@id="infoTable"]/table/tbody/tr[2]/td[2]/div/a'));
    var directors   = parseListOfLinks(_x('//*[@itemprop="director"]/a'));
    var genres      = parseListOfLinks(_x('//*[@itemprop="genre"]/a'));
    var actors      = parseListOfLinks(_x('//*[@id="actorList"]/ul[1]/li/a'));
    var writers     = parseListOfLinks($('td.type:contains(сценарий)').next('td').find('a'));

    var rating      = $('meta[itemprop="ratingValue"]').attr('content');
    var ratingCount = $('span[itemprop="ratingCount"]').text().replace(/\s+/g, '');
    var imdb_text = $(_x('//*[@id="block_rating"]/div[1]/div[2]')).text();
    var match = imdb_text.match(/imdb\:\s*([\d\.]+)\s*\(([\d\s+]+)\)/i);
    var imdb = match ? {
        rating: (Number)(parseFloat(match[1]).toFixed(2)),
        count: parseInt(match[2].replace(/\s+/g, ''))
    } : null;

    var synopsis = $('div[itemprop="description"]').text();

    var age_class = $('div.ageLimit').prop('class');
    var age_match = age_class ? age_class.match(/age(\d+)/i) : undefined;
    var age_limit = age_match ? parseInt(age_match[1]) : undefined;

    var r_MPAA = $('td[class^="rate_"]').find('a').prop('href');
    var MPAA_match = r_MPAA ? r_MPAA.match(/^.*\/(.*)\/$/i) : undefined;
    var MPAA_rating = MPAA_match ? MPAA_match[1] : undefined;

    var runtime = $('td#runtime').text().match(/(\d+)\s+мин/i);
    runtime = runtime ? parseInt(runtime[1]) : undefined;


    //TODO: dates
    var release_date, release_date_rus;

    var release_date_raw = $('.prem_ical[data-ical-type=world]').attr('data-date-premier-start-link');
    var release_date_rus_raw = $('.prem_ical[data-ical-type=rus]').attr('data-date-premier-start-link');
    if (release_date_raw) {
        var m1 = release_date_raw.match(/(\d\d\d\d)(\d\d)(\d\d)/i);
        release_date = m1 ? new Date(m1[1], m1[2]-1, m1[3], 0, 0, 0, 0).getTime() : undefined;
    }
    if (release_date_rus_raw) {
        var m2 = release_date_rus_raw.match(/(\d\d\d\d)(\d\d)(\d\d)/i);
        release_date_rus = m2 ? new Date(m2[1], m2[2]-1, m2[3], 0, 0, 0, 0).getTime() : undefined;
    }

    //other links
    var other_films = [];
    var other_films_hash = {};
    $('a[href^="/film/"]').each(function(el){
        var url = $(this).attr('href');
        var m = url.match(new RegExp("/film/(\\d+)/$"));
        if (m) {
            if (m[1]==movieId) return;      //skip self id
            if (!other_films_hash[m[1]]) {
                other_films.push(m[1]);
                other_films_hash[m[1]] = true;
            }
        }
    });


    return {
        movie:{
            _id: movieId,
            title: $.trim(title),   //document.title,
            title_original: $.trim(title2),
            year: parseInt(year),
            year_end: year_end,
            release: release_date,
            release_rus: release_date_rus,
            slogan: slogan == '-' ? null : slogan,
            countries: countries,
            directors: directors,
            writers: writers,
            genres: genres,
            actors: actors,
            poster: poster,
            serie: serie,
            kinopoisk: {
                rating: (Number)(parseFloat($.trim(rating)).toFixed(2)),
                count: parseInt($.trim(ratingCount).replace(/\s+/g, ''))
            },
            imdb: imdb,
            synopsis: synopsis,
            age_limit: age_limit,
            MPAA_rating: MPAA_rating,
            runtime: runtime
        },
        ids: other_films
    };
}

function preparePage(){
    var page = require('webpage').create();
    page.settings.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36';
    page.customHeaders = {
        'Referer': 'http://www.kinopoisk.ru/popular',
        'Accept-Language': 'ru,en-US;q=0.8,en;q=0.6,bg;q=0.4'
    };
    return page;
}

function realOpenPage(url, waitSelector, done) {
    var page = preparePage();

    /*page.onResourceRequested = function (request) {
        console.log('Request ' + JSON.stringify(request, undefined, 4));
    };*/

    var pageStatus = false;
    page.open(url, function (status) {
        //pageStatus = page.status;
        if (page.content.match(/404\s+\-\s+Страница не найдена/)){
            pageStatus = 404;
        }
        /*if (status !== 'success') {
            page.close();
            done(status, 400);
        } else {
        }*/
        //console.log(status);
    });

// Wait for 'selector' to be visible
    utils.waitFor(function() {
        return page.evaluate(function(selector) {
            return $(selector).is(":visible");
        }, waitSelector);
    }, function(ok) {
        if (!ok) {
            console.log(page.content);
            page.close();
            return done("element wait timeout", pageStatus ? pageStatus : 408);
        }

        done(null, 200, page);
    }, 15000);
}


exports.ParseKinoPoiskLinks = function(url, selector, done) {
    realOpenPage(url, selector, function(err, code, page){
        if (err) {
            return done(err, code);
        }

        console.log('we here');
        var data = page.evaluate(ExtractFilmIds);

        page.close();
        return done(null, code, data);
    });
};


exports.ParseAllLinks = function(url, selector, done) {
    realOpenPage(url, selector, function(err, code, page){
        if (err) {
            return done(err, code);
        }

        var data = page.evaluate(ExtractAllUrls);

        page.close();
        return done(null, code, data);
    });
};


exports.ParseKinoPoiskMovie = function(id, done) {
    realOpenPage('http://www.kinopoisk.ru/film/'+id+'/', ".film-img-box", function(err, code, page){
        if (err) {
            return done(err, code);
        }

        var data = page.evaluate(ExtractData, id);

        page.close();
        done(null, code, data);
    });
};