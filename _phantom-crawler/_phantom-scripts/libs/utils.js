/**
 * User: rarog
 * Date: 11.07.13
 * Time: 21:22
 */
exports.waitFor = function (testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 10000, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                clearInterval(interval); //< Stop this interval
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout");
                    if (typeof(onReady) === "function") onReady(false); //< Do what it's supposed to do once the condition is fulfilled
                    //phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    if (typeof(onReady) === "function") onReady(true); //< Do what it's supposed to do once the condition is fulfilled
                }
            }
        }, 500); //< repeat check every 500ms
};