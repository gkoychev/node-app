/**
 * User: gkoy
 * Date: 11.07.13
 * Time: 16:26
 */
var system = require('system'),
    url = require('./libs/url'),
    parser = require('./parser');

var server = require('webserver').create();
var port = system.args[1] || 8080;

server.listen(port, function(request, response) {

    if (request.method!="GET") {
        response.statusCode(405);
        response.write('Not Allowed Here');
        return response.close();
    }

    var uri = url.parseUri(request.url);
    switch(uri.path){
        default:
            reportError(response, 'Not Found', 404);
            break;

        case "/collect-links":
            if (!uri.queryKey.url) {
                return reportError(response, "url not specified");
            }
            parser.ParseKinoPoiskLinks(decodeURIComponent(uri.queryKey.url), ".logo", function(err, code, res){
                if (err) {
                    return reportError(response, err, code);
                }
                reportSuccess(response, res);
            });
            break;

        case "/collect-all-links":
            if (!uri.queryKey.url) {
                reportError(response, "url not specified");
                return;
            }
            parser.ParseAllLinks(decodeURIComponent(uri.queryKey.url), "img.logo", function(err, code, res){
                if (err) {
                    return reportError(response, err, code);
                }
                reportSuccess(response, res);
            });
            break;

        case "/parse-movie":
            if (!uri.queryKey.id) {
                reportError(response, "id not specified");
                return
            }

            parser.ParseKinoPoiskMovie(uri.queryKey.id, function(err, code, res){
                if (err) {
                    return reportError(response, err, code);
                }
                reportSuccess(response, res);
            });
            break;
    }
});





function reportSuccess(response, res){
    response.statusCode = 200;
    response.write(JSON.stringify(res, null, 4));
    response.close();
}

function reportError(response, err, code){
    response.statusCode = code || 400;
    response.write(JSON.stringify(err));
    response.close();
}