/**
 * User: gkoychev
 * Date: 05.04.13
 * Time: 15:54
 */
var rate    = require('express-rate'),
    redisClient = require("../../common/helpers/redis-client");

var redisHandler = new rate.Redis.RedisRateHandler({client: redisClient});

var mongoose = require('mongoose'),
    User = mongoose.model('User');



exports.applyRateLimit = rate.middleware({
    handler: redisHandler,
    limit: 100,
    interval: 10,
    getRemoteKey: function (req) {
        //console.log(req.connection.remoteAddress);
        return req.query["api_key"];
    }
});

exports.checkUserApiKey = function (req, res, next) {
    if (typeof(req.query["api_key"])=="undefined") {
        return res.json(400, {message: "api key required"});
    }
    //check key in DB
    User.getUserByApiKey(req.query["api_key"], function(err, user){
        if (err) return res.json(500, err);
        if (user==null)
            return res.json(403, {message: "api key not valid"});
        req.user = user;
        return next();
    });
};

exports.loadUserIfApiKeyPresent = function (req, res, next) {
    if (req.query["api_key"]) {
        //check key in DB
        User.getUserByApiKey(req.query["api_key"], function(err, user){
            if (user!=null){
                req.user = user;
            }
            return next();
        });
    } else
        return next();
};

exports.checkKey = function (req, res, next) {
    if (typeof(req.query["api_key"])=="undefined") {
        return res.json(403, {message: "specify api key"});
    }

    return next();
};