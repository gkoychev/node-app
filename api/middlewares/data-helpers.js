/**
 * User: gkoychev
 * Date: 12.04.13
 * Time: 18:31
 */

var mongoose = require('mongoose');
var Movie = mongoose.model('Movie');

exports.loadMovie = function (req, res, next) {
    if (typeof(req.params["id"])=="undefined") {
        return res.json(400, {message: "movie id required"});
    }
    var id = req.params["id"];

    Movie.findOne({_id: id}).lean().exec(function(err, movie) {
        if (err) return res.json(500, err);
        if (!movie) return res.json(404, { message: 'not found' });
        req.movie = movie;

        //TODO: load My rate if logged-in user
        if (req.user) {
            //console.log('user '+req.user._id+' loading movie: '+movie._id);
            req.user.getMovieRate(movie._id, function(err, rate){
                if (!err) {
                    req.movie.user_rate = rate;
                    //console.log('req.movie.user_rate = ' + req.movie.user_rate);
                    return next();
                }
            });
        } else
            return next();
    });
};

