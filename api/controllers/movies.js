/**
 * User: gkoychev
 * Date: 05.04.13
 * Time: 10:48
 */

var config = require('../../config');
var mongoose = require('mongoose');
var Movie = mongoose.model('Movie');
//var User = mongoose.model('User');

/*
 * GET movie by id.
 */
exports.get = function(req, res){
    return res.json(req.movie);
};

exports.rate = function(req, res){
    var rate = parseFloat(req.param('rate'));
    var user = req.user;
    var movie = req.movie;

    //save rate
    user.saveMovieRate(movie._id, rate, function(err){
        if (err)
            return res.json(500, err);
        else
            res.json(true);

        user.triggerRatesProcessing();
    });
};

exports.getRate = function(req, res){
    var user = req.user;
    var movie = req.movie;

    user.getMovieRate(movie._id, function(err, rate){
        if (err)
            return res.json(500, err);
        else
            res.json({rate: rate == undefined ? "": rate});
    });
};

exports.find = function(req, res){
    var query = req.param('query');

    var q = Movie.find().regex('title', new RegExp(query, 'i')).lean().limit(10);

    q.exec(function(err, docs) {
        if (err) return res.json(500, err);
        if (!docs) return res.json(404, { message: 'not found' });
        return res.json(docs);
    });
};