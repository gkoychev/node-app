/**
 * User: gkoychev
 * Date: 12.04.13
 * Time: 17:55
 */

var config = require('../../config'),
    mongoose = require('mongoose'),
    Advice = mongoose.model('Advice'),
    Movie = mongoose.model('Movie'),
    redisClient = require("../../common/helpers/redis-client"),
    async = require('async');

exports.me = function(req, res){

    if (typeof(req.user)=='undefined')
        return res.json(404, {message: 'user not found'});

    return res.json(req.user);
};

exports.process = function(req, res){
    var user = req.user;
    user.triggerRatesProcessing();
    res.json(true);
};

exports.getAdvice = function(req, res){
    var user = req.user;
    var key = Advice.getCacheKey(user.id);

    var start = req.query.start || 0;
    var take = req.query.take || 10;
    var use_filter = req.query.no_filter == undefined;
    if (take > 100 ) take = 100;
    if (take <= 0 ) take = 10;

    var expire = -1;

    //START WATERFALL
    async.waterfall([

        //( 1 ) check for cache
        function(callback) {
            redisClient.ttl(key, function(err, ttl){
                if (err) return callback(true);
                expire = ttl;
                return callback(null, ttl>0);
            });
        },

        //( 2 ) update cache if not present
        function(cachePresent, callback) {
            if (!cachePresent) {
                return Advice.updateRedisCache(user.id, function(err){
                    callback(err, key);
                });
            } else {
                return callback(null, key);
            }
        },

        //( 2.1 ) load all data from redis
        function(new_key, callback) {
            redisClient.lrange(new_key, 0, -1, function(err, data) {
                callback(err, data);
            });
        },

        //( 2.2 ) filter data
        function(items, callback) {
            if (use_filter) {
                if (user.rates==undefined)
                    user.rates = [];
                items = items.filter(function(item) {
                    return (typeof(user.rates[item])=="undefined");
                });
            }
            callback(null, items);
        },

        //( 2.3 ) get needed part of data from array
        function(items, callback) {
            var total = items.length;
            var part = items.slice(+start, +start + +take );
            callback(null, {
                items: part,
                total: total
            });
        },

        /*
        //( 3 ) load needed part of data from redis
        function(new_key, callback) {

            async.parallel({
                items: function(cb) {
                    redisClient.lrange(new_key, +start, +start + +take - 1, function(err, data) {
                        //console.log([ +start, +start + +take - 1]);
                        //console.log(data);
                        if (err) return cb(true);
                        return cb(null, data);
                    });
                },
                total: function(cb){
                    redisClient.llen(new_key, function(err, data) {
                        if (err) return cb(true);
                        return cb(null, data);
                    });
                }
            },
            function(err, res) {
                console.log(res);
                callback(err, res);
            });
        },*/

        //( 4 ) read details for movies
        function(data, callback) {
            Movie.getByIdsWithOrder(data.items, function(err, res){
                data.items = res;
                callback(err, data);
            });
        },

        //( 5 ) update cache if it is expired soon
        function(data, callback) {
            callback(null, data);

            //refresh soon expired cache
            if (expire < config.caching.advice_cache_prefetch_ttl)
                Advice.updateRedisCache(user.id, function(){});
        }

    ], function(err, result){
        result.start = start;
        result.take = take;
        if (+start + +take >= result.total)
            result.finished = true;
        return res.json(result);
    });

};


