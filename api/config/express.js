var express = require('express');

module.exports = function (app, config) {

    // development only
    if (config.dev) {
        app.use(express.errorHandler());
        app.use(express.logger('dev'));
    }

    // all environments
    app.use(express.compress());
    app.use(express.bodyParser());
    app.use(express.methodOverride());

    app.use(app.router);
};