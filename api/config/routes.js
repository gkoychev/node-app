var apiKey = require("../middlewares/api-key")
  , dataHelpers = require("../middlewares/data-helpers");

module.exports = function (app, config) {
    //routes

    var movies = require(config.root + '/api/controllers/movies');
    var users = require(config.root + '/api/controllers/users');


    /*
     * Movie related API
     */
    app.get('/api/movie/:id',
        //apiKey.checkKey,
        //apiKey.applyRateLimit,        // requires api_key
        apiKey.loadUserIfApiKeyPresent,
        dataHelpers.loadMovie,
        movies.get);

    app.get('/api/movie/find/:query', /*apiKey.checkKey, apiKey.applyRateLimit,*/ movies.find);

    /*
     * User related API
     */
    app.get('/api/me', apiKey.checkUserApiKey, apiKey.applyRateLimit, users.me);
    app.get('/api/process', apiKey.checkUserApiKey, apiKey.applyRateLimit, users.process);
    app.get('/api/advice', apiKey.checkUserApiKey, apiKey.applyRateLimit, users.getAdvice);

    //TODO: post needed here
    app.get('/api/movie/:id/rate/:rate',
            apiKey.checkUserApiKey,
            apiKey.applyRateLimit,
            dataHelpers.loadMovie,
        movies.rate);

    app.get('/api/movie/:id/rate',
        apiKey.checkUserApiKey,
        apiKey.applyRateLimit,
        dataHelpers.loadMovie,
        movies.getRate);
};
