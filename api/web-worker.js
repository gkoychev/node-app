/**
 * User: rarog
 * Date: 04.04.13
 * Time: 21:17
 */
module.exports = function (config) {

    var express = require('express')
      , mongooseInit = require('../common/helpers/mongoose-init');

    // Bootstrap db connection & models
    mongooseInit(config, {
        models_path: 'common/model',
        models: ['movie', 'user', 'advice']
    });

    // setup express
    var app = express();
    require('./config/express')(app, config);
    require('./config/routes')(app, config);

    //run app
    app.listen(config.api.port);

    console.log('API server listening on port ' + config.api.port);
};