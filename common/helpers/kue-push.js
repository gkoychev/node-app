/**
 * User: rarog
 * Date: 18.04.13
 * Time: 23:08
 */
var kue = require('kue-mod')
    ,  jobs = kue.createQueue()
    , redis = require('redis');

kue.redis.createClient = function() {
    //console.log('create redis for kue');
    return redis.createClient();
};

module.exports = function (type, job) {
    jobs.createJob(type, job).save();
};


