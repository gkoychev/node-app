/**
 * User: gkoychev
 * Date: 12.04.13
 * Time: 12:03
 */
var mongoose = require('mongoose'),
    mongooseRedisCache = require("mongoose-redis-cache"),
    path = require('path');


module.exports = function (config, options, done) {

    mongoose.set('debug', config.dev);
    if (options.noDebug) {
        mongoose.set('debug', false);
    }

    var models_path = options.models_path || 'models';
    var models = options.models || [];

    for(var i=0; i<models.length; i++)
    {
        var model = path.join(config.root, models_path, models[i]);
        require(model)(config);
    }

    mongoose.connect(config.mongo.connectionString, function(err) {
        if (err) {
            console.log(err);
            return process.exit(config.consts.worker_fatal_exit_code);
        }
        if (typeof done == 'function')
            done();
    });

    mongooseRedisCache(mongoose);
};