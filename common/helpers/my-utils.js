/**
 * User: gkoy
 * Date: 26.06.13
 * Time: 16:27
 */

Array.prototype.diff = function(a) {
    return this.filter(function(i) {return !(a.indexOf(i) > -1);});
};