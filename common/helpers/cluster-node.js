var cluster = require('cluster')
    , path = require('path');


function workerFork(cluster, config){
    var worker = cluster.fork(config);
    worker.wConfig = config;
    return worker;
}


module.exports = function (config, options) {

    module.exports.cluster = cluster;

    var autoRestart = options.autoRestart || false;

    if (cluster.isMaster) {

        //start workers
        for (var k in options.workers) {
            var wConfig = options.workers[k];
            for (var i = 0; i < (wConfig.count || 1); i++) {
                workerFork(cluster, wConfig);
            }
        }

        //handle exit
        cluster.on('exit', function(worker) {
            var exitCode = worker.process.exitCode;
            if (exitCode==config.consts.worker_super_fatal_exit_code) {
                console.log('worker have send exiting code');
                return process.exit(1);
            }

            console.log('worker ' + worker.process.pid + ' died ('+exitCode+'). restarting...');
            if (worker.suicide === true || exitCode==config.consts.worker_fatal_exit_code) {
                console.log('Oh, it was just suicide\' – no need to worry');
            } else {
                if (autoRestart) {
                    workerFork(cluster, worker.wConfig);
                }
            }
        });

    } else {
        //load worker
        console.log('we forked: ' + process.env.workerSrc);
        require(path.join(config.root, process.env.workerSrc))(config);
    }
};