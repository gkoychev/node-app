/**
 * User: gkoychev
 * Date: 04.04.13
 * Time: 15:51
 */
var redis = require("redis")
  , config = require("../../config");

module.exports = function () {
    var client = new redis.createClient(config.redis.port, config.redis.host);
    //module.exports.client = client;
    console.log('init redis client');
    return client;
}();
