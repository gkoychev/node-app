/**
 * User: rarog
 * Date: 27.09.13
 * Time: 23:05
 */

var path = require('path');

module.exports = function(config){

    this.getMoviePosterPaths = function(id) {

        var part = id.toString().substring(0,2);
        var savePath = path.join(config.root, config.paths.poster_path, part);

        var fileName = id + '.jpg';

        return {
            savePath: savePath,
            fileName: fileName,
            fullPath: path.join(savePath, fileName),
            dbPath: path.join(config.paths.urls.poster_base_url, part, fileName)
        };
    }
};