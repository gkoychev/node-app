/**
 * User: gkoy
 * Date: 04.07.13
 * Time: 17:19
 */

exports.CollectMovieRatingStats = function(ratedMovies, rates, logCallback) {

    if (!logCallback) logCallback = function(){};

    var section, item;
    var results = {genres:{}, actors:{}, directors:{}, countries:{}};

    for(var i=0; i<ratedMovies.length; i++) {           // iterate through movies
        var movie = ratedMovies[i];

        for(section in results) {
            if (results.hasOwnProperty(section)) {      // iterate through stats categories

                //Array property
                if (Array.isArray(movie[section])){
                    // iterate Array properties for movie [ genres / actors / directors ]
                    var cnt = 0;
                    for(var j=0; j<movie[section].length; j++) {
                        item = movie[section][j];
                        if (item=='') continue;
                        if (!results[section][item]) results[section][item] = [];
                        results[section][item].push(rates[movie._id]);
                        cnt++;
                        if (section=='directors' && cnt>=2) break; //use only first 2 directors from movie
                        //if (section=='countries' && cnt>=5) break; //use only first 2 countries from movie
                        if (section=='actors' && cnt>=4) break; //use only first 4 actors from movie
                    }
                }
                else if (section=='decades'){
                    item = movie['year'] - movie['year'] % 10;
                    //push
                    if (!results[section][item]) results[section][item] = [];
                    results[section][item].push(rates[movie._id]);
                }

                /*
                //String property
                if (typeof movie[section]=='string'){
                    item = movie[section];
                    if (item=='') continue;
                    if (!results[section][item]) results[section][item] = [];
                    results[section][item].push(rates[movie._id]);
                }
                */
            }
        }
    }
    //console.log('CollectMovieRatingStats');
    //console.log(results);

    //calc averages
    for(section in results) {
        if (results.hasOwnProperty(section)){
            for(item in results[section]) {
                if (results[section].hasOwnProperty(item)){
                    var arr = results[section][item];

                    //console.log("Section: %s, Item: %s", section, item);
                    //console.log(arr);

                    //get average rating for an item
                    var sum = 0;
                    for(var a=0;a<arr.length;a++)
                        sum += arr[a];
                    var avg = arr.length==0? 0 : sum/arr.length;

                    //INFO: use hyperbola to correct low rates: y(x) = 1 + 1/(-1 - 0.5x)
                    var koeff = 1.0 + 1.0/(-1.0 - 0.5*arr.length);
                    logCallback("correct "+section+" "+item+" ["+arr.length+"]"+" ("+avg.toFixed(2)+" by "+koeff.toFixed(2)+") = " + (avg*koeff).toFixed(2));
                    avg *= koeff;

                    results[section][item] = avg;
                }
            }
        }
    }
    return results;
};



exports.CalculateMovieScore = function(movie, stats, logCallback) {

    if (!logCallback) logCallback = function(){};

    var imdb_rating = +movie.imdb.rating || 0;
    var kinopoisk_rating = +movie.kinopoisk.rating || 0;

    var movieRating = kinopoisk_rating;
    //if (imdb_rating) {
        movieRating = (kinopoisk_rating + imdb_rating)/2;
    //}

    logCallback("KP rating: " + movie.kinopoisk.rating);
    logCallback("IMDB rating: " + imdb_rating);
    logCallback("average rating: " + movieRating);

    //var score = (movieRating>=5)? movieRating * 2 : movieRating;
    //rates[id] = (2*(rates[id] - min) / (max - min)) - 1;
    var score = movieRating / 2;
    logCallback("initial rating: " + score);
    /*
    //normalize score
    var min = 0, max = 4.9;
    score = (2*(score) / (max)) - 1;
    logCallback("normalized rating: " + score);

    var n_init_rate_mult = 5;
    logCallback("\nscore = " + score + "*"+n_init_rate_mult+"=\n"+(score*n_init_rate_mult)+" (film rate)");
    score *= n_init_rate_mult; //make score more important
    */

    //run for genres actors and directors
    for(var section in stats) {
        if (stats.hasOwnProperty(section)){

            //get uniq
            var items = movie[section].filter(function(a){return !this[a] ? this[a] = true : false;}, {});

            var len = items.length;
            if (section == "genres" && len>2) len = 2;
            if (section == "countries" && len>1) len = 1;   //use 3 countries from movie

            for(var i=0; i<len; i++) {
                if (section=='actors' && i>=5)
                    break;
                if (stats[section][items[i]]!=undefined) {
                    var partial_score = stats[section][items[i]];

                    if (section=='genres' || section=='directors')
                        partial_score /= 3;
                    /*if (section=='countries')
                        partial_score *= 0.8;*/

                    logCallback("+ " + partial_score.toFixed(3)+" ("+items[i]+")");
                    score += partial_score;
                }
            }

        }
    }
    logCallback("= " + score);
    return score;
};
