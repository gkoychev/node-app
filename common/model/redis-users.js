/**
 * User: gkoychev
 * Date: 05.04.13
 * Time: 18:31
 */
//var redisClient = require("../modules/redis-client");
var redis = require("redis")
    , config = require("../../config")
    , crypto = require('crypto');

var client = new redis.createClient(config.redis.port, config.redis.host);

var userlist_key_prefix = "users:";

var Users = new function(){
    //console.log('create Users object');

    function _getUserProviderKey (provider, id){
        return userlist_key_prefix + provider + ":" + id;
    }

    function getUserListKeyByProviderKey (providerKey){
        return userlist_key_prefix + crypto.createHash('md5').update(providerKey).digest("hex");
    }

    function _findByProvider (provider, id, done) {
        console.log("find user");
        var provider_key = userlist_key_prefix + provider + ":" + id;

        client.get(provider_key, function (err, userListKey) {
            if (err || !userListKey)
                return done(err, null);

            console.log("find key: " + userListKey);

            _getByKey(userListKey, function (err, res) {
                done(err, res);
            });
        });
    }

    function _getByKey (userListKey, done) {
        console.log("find user by hash: "+ userListKey);
        client.hgetall(userListKey, function (err, res) {
            if (err || !res) { return done(err, res); }
            res.key = userListKey;
            done(err, res);
        });
    }

    function _addUser (userProviderKey, user, done){
        console.log("save user");

        var userListKey = getUserListKeyByProviderKey(userProviderKey);

        client.set(userProviderKey, userListKey);           //update provider hash
        client.hmset(userListKey, user, function (err) {    //update user object
            user.key = userListKey;
            done(err, userListKey);
        });
    }

    function _proposeLinkProfile(user, newProfile){
        console.log("!!!!!proposeLinkProfile");

        /*
         link accounts
         TODO: ask user about his wish
         */
        var userProviderKey = _getUserProviderKey(newProfile.provider, newProfile.id);
        client.set(userProviderKey, user.key);

        //add new acc to user profile
        client.hset(user.key, newProfile.provider, JSON.stringify(newProfile._json));
    }

    return {
        addUser: _addUser,
        getByKey : _getByKey,
        findByProvider: _findByProvider,
        getUserProviderKey: _getUserProviderKey,
        proposeLinkProfile: _proposeLinkProfile
    }
};

 module.exports = Users;