var crypto = require('crypto')
  , mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , redisClient = require("../helpers/redis-client");

module.exports = function (config) {

    var UserSchema = new Schema({

        name: {type: String, index: true},

        email: {type: String, index: true},
        gender: String,
        avatar: String,

        api_key: {type: String, index: { sparse: true, unique: true }},

        facebook: {},
        twitter: {},
        vkontakte: {},

        stats: {},
        logs: String,

        rates: {}

    }, { versionKey: false });

    function getCacheKey (value){
        return 'cache:user:'+value;
    }

    //used mostly by API router
    UserSchema.statics.getUserByApiKey = function(api_key, done){
        var cacheKey = getCacheKey(api_key);
        var _this = this;
        redisClient.get(cacheKey, function(err ,res){
            if (res != null) {
                console.log('getUserByApiKey: from REDIS');
                var obj = JSON.parse(res);
                var model = obj==null? null : new _this(JSON.parse(res));
                done(err, model);
            } else {
                //console.log('getUserByApiKey: from MONGO');
                _this.findOne({ api_key: api_key }, function(err, res){
                    done(err, res);
                    //update redis cache
                    redisClient.set(cacheKey, JSON.stringify(res));
                    redisClient.expire(cacheKey, config.caching.api_key);
                });
            }
        });
    };

    UserSchema.statics.getUserById = function(id, done){
        this.findOne({ _id: id }, function(err, res){
            done(err, res);
        });
    };

    UserSchema.statics.getRatesByUserId = function(id, done){
        this.findOne({ _id: id }, { rates: 1 }, function(err, res){
            done(err, res);
        });
    };

    UserSchema.methods.generateApiKey = function(){
        this.api_key = crypto.createHash('md5').update(this._id.toString() + Date.now()).digest('hex');
    };

    UserSchema.methods.getToken = function(){
        return this.api_key;
    };

    UserSchema.methods.triggerRatesProcessing = function(){
        //trigger kue processing
        var kue_push = require('../helpers/kue-push');
        kue_push(config.consts.kue_job_type_calc, {
            uid: 'calc:user:'+this.id,
            userid: this.id,
            title: 'calc stats for user: '+this.name
        });
    };

    UserSchema.methods.saveMovieRate = function(movieId, rate, done){
        var upd = {$set: {}};
        upd.$set['rates.'+movieId] = rate;
        this.model('User').update({_id: this.id}, upd, done);
        var cacheKey = getCacheKey(this.api_key);
        console.log(cacheKey);
        redisClient.del(cacheKey);
    };

    UserSchema.methods.getMovieRate = function(movieId, done){
        this.model('User').findOne({_id: this.id}).select('rates.'+movieId).exec(function(err, doc){
            if (!doc.rates) doc.rates = [];
            console.log(doc.rates);
            if (err)
                return done(err);
            done(null, doc.rates[movieId]);
        });
    };


    mongoose.model('User', UserSchema);
};


