var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    util = require('util');

module.exports = function (config) {

    var MovieSchema = new Schema({
        _id: Number,
        title: {type: String, index: true},
        title_original: String,
        year: Number,
        year_end: Number,
        serie: Boolean,
        actors: [String],
        directors: [String],
        writers: [String],
        genres: [String],
        countries: [String],
        imdb: {
            count: Number,
            rating: Number
        },
        kinopoisk: {
            count: Number,
            rating: Number
        },
        age_limit: Number,
        MPAA_rating: String,
        runtime: Number,  //in minutes

        poster: String,
        poster_local: String,
        slogan: String,
        synopsis: String,
        updated: { type: Number, default: new Date().getTime() },
        release: Number,
        release_rus: Number

    }, { versionKey: false });

    MovieSchema.virtual('movieUrl').get(function(){
        return util.format(config.paths.urls.movie_url_pattern, this._id);
    });
    MovieSchema.set('toJSON', { getters: true, virtuals: true });

    MovieSchema.statics.getByIds = function(ids, done){
        var query = this.find({ _id: {$in: ids} });
        query.exec(done);
    };

    MovieSchema.statics.getByIdsWithOrder = function(ids, done){
        var els = [];
        ids.forEach(function(id){
            els.push({_id: id});
        });
        var query = this.find({$or: els});
        query.exec(done);
    };

    MovieSchema.set('redisCache', true);

    mongoose.model('Movie', MovieSchema);
};