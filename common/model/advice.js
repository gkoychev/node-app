/**
 * User: gkoychev
 * Date: 15.05.13
 * Time: 11:20
 */
var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , redisClient = require("../helpers/redis-client");


module.exports = function (config) {

    var AdviceSchema = new Schema({
        advice: [Number]
    },{
        versionKey: false,
        collection: 'advices'
    });

    AdviceSchema.statics.getCacheKey = function(userId){
        return 'cache:advice:' + userId;
    };
    AdviceSchema.statics.getCacheTmpKey = function(userId){
        return '~tmp_cache:advice:' + userId;
    };
    AdviceSchema.statics.getCacheUniqKey = function(userId){        //???
        return 'cache:advice:' + (+new Date()).toString(36) + ':' + userId;
    };

    //load advice from mongo and store to redis
    AdviceSchema.statics.updateRedisCache = function(id, done) {
        console.log('updateRedisCache');
        var key = this.getCacheKey(id);
        var tmpKey = this.getCacheTmpKey(id);

        this.findOne({_id: id}, function(err, data){
            if (err) return done(true);

            if (data==null || !data.advice || data.advice.length==0)
                return done(null);

            //load to redis cache
            return arrayToRedis(key, tmpKey, data.advice, done);
        });
    };

    mongoose.model('Advice', AdviceSchema);


    function arrayToRedis(key, tmpKey, array, done) {
        redisClient.del(tmpKey, function() {
            var len = array.length;
            if (len==0) return done(null);

            for(var i=0; i<len; i++ ) {
                if (i==len-1) {
                    //last item
                    redisClient.rpush(tmpKey, array[i], function() {
                        redisClient.rename(tmpKey, key, function(){
                            redisClient.expire(key, config.caching.advice_cache_lifetime);
                            console.log('done');
                            return done(null);
                        });
                    });
                }
                else {
                    redisClient.rpush(tmpKey, array[i]);
                }
            }
        });
    }

};