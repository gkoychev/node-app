/**
 * User: gkoi
 * Date: 10.06.13
 * Time: 14:37
 */
define([
    'can',
    'can/model'

], function(can) {
    'use strict';

    return can.Model({

        //model class members
        total: 0,
        start: 0,
        allLoaded: false,

        findAll: 'POST /advice/get',

        //reformat data to can canjs model standards
        models : function(data){
            this.total = data.total;
            if (data.finished === true)
                this.allLoaded = true;
            return can.Model.models.call(this, data.items);
        },

        loadMore: function(take, cb){
            this.findAll( {start: this.start, take: take}, function( items ){
                if (cb)
                    cb(items);
            });
            this.start += take;
        }
    }, {
        //loaded items class members
        test: function(){
            alert('here');
        }
    });
});