/**
 * User: rarog
 * Date: 23.04.13
 * Time: 23:45
 */

requirejs.config({
    //appDir: ".",
    baseUrl: "/js/app",
    paths: {

        /* Load jquery from google cdn. On fail, load local file. */
        jquery: ['//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min','/js/lib/jquery.min'],

        /* Load bootstrap from cdn. On fail, load local file. */
        bootstrap: ['//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.1/js/bootstrap.min','/js/lib/bootstrap.min'],

        hogan: '../lib/hogan',
        text: '../lib/require/text',
        hgn: '../lib/require/hgn',

        can: '../lib/canjs/can',
        templates: './templates'

    },
    shim: {
        /* Set bootstrap dependencies (just jQuery) */
        'bootstrap' : ['jquery']
    }
});

require([
    'config',
    'bootstrap'         //globally for menu

], function(conf){
    'use strict';

    require(['pages/' + conf.section + '_page']);
});