/**
 * User: rarog
 * Date: 15.06.13
 * Time: 1:03
 */
define([
    'jquery',
    'settings',
    'controls/rating_control',

], function ($, settings, RatingControl) {
    'use strict';

    String.prototype.format = function() {
        var formatted = this;
        for(var arg in arguments) {
            formatted = formatted.replace("{" + arg + "}", arguments[arg]);
        }
        return formatted;
    };

    return {
        initStarsControl: function(elements, rating){
            //attach rating control
            return new RatingControl(elements, {
                rating: rating,
                onUserChangeRating: function(control, new_rate){
                    var movieId = $(control.element).data('id');
                    $.ajax({
                        type: "POST",
                        url: settings.movieRating.saveUrl.format(movieId),
                        cache: false,
                        data: {rating: new_rate}
                    });
                }
            });
        }
    };
});