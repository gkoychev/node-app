/**
 * User: gkoi
 * Date: 10.06.13
 * Time: 14:57
 */
define([
    'settings',
    'can',
    'can/control',
    'models/advice_model',
    'hgn!templates/movie',
    'utils/utils'

], function (settings, can, Control, AdviceModel, movie_template, utils) {
    'use strict';

    return can.Control({
        init: function( /*element, options*/ ) {
            can.extend(this.options, settings.movielist);

            this.loading = false;
            this.loadMore(this.options.loadFirstCount, true);
            this.total = 0;
        },

        '{window} scroll': function(){
            if($(window).scrollTop() + $(window).height() > $(document).height() - this.options.bottomProximityOffset) {
                var delta = this.total % 4 === 0 ? 0 : 4 - this.total % 4;   //we need full rows
                //console.log("delta = %d", delta);
                this.loadMore(this.options.loadMoreCount + delta);
            }
        },

        '.overlay.bottom click': function(){
            return false;
        },

        loadMore: function(count, doClear){
            var that = this;
            if (this.loading === true) return;
            this.loading = true;
            AdviceModel.loadMore(count, function(items){
                that.loading = false;
                if (doClear)
                    that.element.empty();
                if (!items)
                //TODO: show some message if no recommendations available
                    return;

                $.each(items, function(i, item){
                    that._processItem(item);
                });
            });
        },


        /* processing one movie item from json */
        _processItem: function(item) {
            //check for existence
            if (this.element.find('.movie-item[data-id='+item._id+']').length >0 )
            {
                console.log('found duplicate: '+item._id);
                return;
            }

            var movie = $(movie_template({movie: item}));

            //attach rating control
            utils.initStarsControl(movie.find('.stars'));

            this.element.append(movie);
            this.total++;
            //console.log("%d - %s", this.total, item._id);
        }
    });
});