/**
 * User: gkoy
 * Date: 27.06.13
 * Time: 16:02
 */
define([
    'can'

], function (can) {
    'use strict';

    return can.Control({

        defaults: {
            StarElement: 'span',
            stars: 10,
            stars_class: 'star-rating',
            active_class: 'active',

            onUserChangeRating: function(){}
        }

    },{
        init: function( /*element, options*/ ) {
            var els = [];
            for (var i=0; i<this.options.stars; i++) {
                els.push('<span></span>');
            }
            this.element
                .empty()
                .addClass(this.options.stars_class)
                .append(els.join(''));

            var rating = $(this.element).data('rating');
            if (!rating)
                rating = this.options.rating;

            //console.log(rating);

            //show current rating
            if (rating) {
                this.element.find('span:nth-last-child('+rating+')')
                    .addClass(this.options.active_class);
            }
        },

        '{StarElement} click': function(el, e){

            //preserve view with class
            $(e.target)
                .addClass('active')
                .siblings()
                .removeClass('active');

            //count stars
            var stars = $(e.target).nextAll('span').length + 1;

            this.options.onUserChangeRating(this, stars);

            return false;
        }
    });
});