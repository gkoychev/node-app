/**
 * User: rarog
 * Date: 15.06.13
 * Time: 1:38
 */
define([
], {

    movielist: {
        loadFirstCount: 12,
        loadMoreCount: 8,
        bottomProximityOffset: 200
    },

    movieRating: {
        saveUrl: '/movie/{0}/rate'
    }

});