/**
 * User: rarog
 * Date: 24.05.13
 * Time: 0:11
 */
define([
    'jquery',
    'can',
    'can/route',
    'controls/advice_control'

], function($, can, route, AdviceControl) {
    'use strict';

    $(document).ready(function(){
        new AdviceControl('#advices', {a: 'b'});
    });

    //TODO: init advice page
    console.log('TODO: init advice page');
});