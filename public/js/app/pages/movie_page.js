/**
 * User: rarog
 * Date: 12.07.13
 * Time: 21:35
 */
define([
    'jquery',
    'settings',
    'utils/utils'

], function($, settings, utils) {
    'use strict';

    $(document).ready(function(){
        //attach rating control
        utils.initStarsControl('.stars');
    });

});