var oneDay = 86400;
var oneYear = oneDay*356;
//var numCPUs = require('os').cpus().length;

var env = process.env.NODE_ENV || 'development';

var constants = require('./constants');
var path = require('path');
var rootPath = path.normalize(__dirname + '/');

module.exports = function() {
    var config = {
        dev: env=='development',
        root: rootPath,
        api: {
            api_connect_host: 'localhost',
            port: 3010,
            workersCount: 2     //numCPUs
        },
        app: {
            port: 3000,
            workersCount: 2,    //numCPUs
            name: 'MovieJuice'
        },
        kue_gui_port: 4000,

        cookieSecret: "sdfsfedd5465464!e",
        sessions: {
            secret: "ziga1488",
            ttl: oneYear
        },

        redis: {
            host: "localhost",
            port: "6379"
        },
        mongo: {
            connectionString: "mongodb://localhost:27017/test"
        },

        caching: {
            advice_cache_lifetime: 3600*24,     //one day
            advice_cache_prefetch_ttl: 3600,    //hour
            api_key: 60         //minute
        },



        twitter: {
            clientID: "djbtPdXW8eWp6TIUW6c1QQ",
            clientSecret: "wZ32Jn5G3t5DtwqSxO9XISYdKAYUJW27XxGxHQ",
            callbackURL: "http://localhost:3000/auth/twitter/callback"
        },
        facebook: {
            clientID: "434406733311875",
            clientSecret: "8760f4d3610e456ccb967162c986b61c",
            callbackURL: "http://localhost:3000/auth/facebook/callback"
        },
        vkontakte: {
            clientID: "3557559",
            clientSecret: "mWIajkqRE00SPiFbiZSw",
            callbackURL: "http://localhost:3000/auth/vkontakte/callback"
        }
    };

    //merge config from constants.js
    for (var key in constants) {
        if (constants.hasOwnProperty(key)) {
            config[key] = constants[key];
        }
    }
    return config;
}();
