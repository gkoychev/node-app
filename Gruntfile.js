module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint : {
            files : ['public/js/app/**/*.js'],
            options : {
                validthis : true,
                laxcomma : true,
                laxbreak : true,
                browser : true,
                eqnull : true,
                debug : true,
                devel : true,
                boss : true,
                expr : true,
                asi : true,
                globals : {
                    jQuery : true
                }
            }
        },
        requirejs: {
            compile: {
                options: {
                    optimize: "none",
                    paths: {
                        "config": "empty:",
                        "jquery": "empty:",
                        "bootstrap": "empty:",
                        can: '../lib/canjs/can',
                        hgn: '../lib/require/hgn',
                        text: '../lib/require/text',
                        hogan: '../lib/hogan'
                    },
                    findNestedDependencies: true,
                    stubModules: ['text', 'hgn'],
                    include: [
                        'pages/advice_page',
                        'pages/movie_page',
                        'pages/index_page'
                    ],
                    //mainConfigFile: 'public/js/app/main.js',
                    name: "main",
                    baseUrl: "public/js/app",
                    out: "public/dist/app.js"
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    //grunt.loadNpmTasks('grunt-contrib-uglify');

    // Default task(s).
    grunt.registerTask('default', ['jshint', 'requirejs'/*, 'uglify'*/]);
};