/**
 * User: rarog
 * Date: 15.06.13
 * Time: 1:51
 */

module.exports.consts = {
    kue_job_type_calc: 'calc',
    worker_fatal_exit_code: 66,
    worker_super_fatal_exit_code: 67,
    processor:{
        advicesSerializeAmount: 1000
    }
};

module.exports.paths = {
    urls: {
        movie_url_pattern: '/movie/%d',
        poster_base_url: '/posters/'
    },
    poster_path: './public/posters/'
};